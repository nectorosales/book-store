import {TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { ConnectionOptions } from 'typeorm';
import { Configuration } from '../config/config.keys';

export const databaseProviders = [
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
     async useFactory(config: ConfigService){
      return {
        ssl: true,
        type: 'mysql',
        port: 3306,
        /*host: config.get(Configuration.HOST),
        username: config.get(Configuration.USERNAME),
        password: config.get(Configuration.PASSWORD),
        database: config.get(Configuration.DATABASE),*/
        host: '192.168.99.100',
        username: 'root',
        password: 'secret',
        database: 'homestead',
        entities: [__dirname + '/../**/*.entity{.ts,.js}'],
        migrations: [__dirname + '/migrations/*{.ts,.js}'],
        synchronize: false,
      } as ConnectionOptions
     }
  })
]
