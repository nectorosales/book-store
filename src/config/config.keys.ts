export enum Configuration {
  PORT = '3000',
  HOST = '192.168.99.100',
  USERNAME = 'root',
  PASSWORD = 'secret',
  DATABASE = 'homestead',
  JWT_SECRET = 'JWT_SECRET'
}
