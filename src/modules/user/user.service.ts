import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { MapperService } from '../../shared/mapper.service';
import { UserDto } from './dto/user.dto';
import { User } from './user.entity';
import { RoleRepository } from '../role/role.repository';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly _userRepository: UserRepository,
    @InjectRepository(RoleRepository)
    private readonly _roleRepository: RoleRepository,
    private readonly _mapperService: MapperService
  ) {

  }

  async getAll(): Promise<User[]>{

    const users = await this._userRepository.find({
      where: { deletedAt: null },
    });

    return users;
  }

  async get(id: number): Promise<UserDto>{
    if(!id) {
      throw new BadRequestException('id must be sent')
    }

    const user = await this._userRepository.findOne(id, {
      where: { deletedAt: null },
    });

    if(!user) {
      throw new NotFoundException();
    }

    return this._mapperService.map<User, UserDto>(user, new UserDto());
  }

  async store(user: User): Promise<UserDto>{
    const savedUser = await this._userRepository.save(user);
    return this._mapperService.map<User, UserDto>(savedUser, new UserDto());
  }

  async update(id: number, user: User): Promise<void>{
    await this._userRepository.update(id, user);
  }

  async destroy(id: number, user: User): Promise<void>{
    const userExist = await this._userRepository.findOne(id, {
      where: { deletedAt: null }
    });

    if(!userExist) {
      throw new NotFoundException();
    }

    await this._userRepository.update(id, {
      deletedAt: new Date()
    });
  }

  async setRoleToUser(userId: number, roleId: number){
    const userExist = await this._userRepository.findOne(userId, {
      where: { deletedAt: null }
    });

    if(!userExist) {
      throw new NotFoundException();
    }

    const roleExist = await this._roleRepository.findOne(roleId, {
      where: { deletedAt: null }
    });

    if(!roleExist) {
      throw new NotFoundException('Role does not exist');
    }

    userExist.roles.push(roleExist);
    await this._userRepository.save(userExist);

    return true;

  }
}
