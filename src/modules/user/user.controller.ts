import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';
import { UserDto } from './dto/user.dto';
import { UserDetails } from './user.details.entity';
import { getConnection } from 'typeorm';
import { Role } from '../role/role.entity';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';

@Controller('users')
export class UserController {
  constructor(private readonly _userService: UserService) {

  }

  @Get()
  @UseGuards(AuthGuard())
  async getUsers(@Param() id: number): Promise<User[]>{
    const users = await this._userService.getAll();
    return users;
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.ADMIN)
  async getUser(@Param('id', ParseIntPipe) id: number): Promise<UserDto> {
    const user = await this._userService.get(id);
    return user;
  }

  @Post()
  async create(@Body() user: User): Promise<UserDto> {
    const details = new UserDetails();
    details.name = "Néctor";
    user.details = details;

    const repo = await getConnection().getRepository(Role);
    const defaultRole = await repo.findOne({ where: { name: 'GENERAL'}});
    user.roles = [defaultRole];

    const createUser = await this._userService.store(user);
    return createUser;
  }

  @Patch(':id')
  async update(@Param('id', ParseIntPipe) id: number, @Body() user: User) {
    await this._userService.update(id, user);
    return true;
  }

  @Delete(':id')
  async delete(@Param('id', ParseIntPipe) id: number, @Body() user: User) {
    await this._userService.destroy(id, user);
    return true;
  }

  @Post('setRole/:userId/:roleId')
  async setRoleToUser(@Param('userId', ParseIntPipe) userId: number, @Param('roleId', ParseIntPipe) roleId: number) {
    return this._userService.setRoleToUser(userId, roleId);
  }
}
