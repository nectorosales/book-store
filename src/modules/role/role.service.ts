import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { RoleRepository } from './role.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './role.entity';
import { CreateRoleDto, ReadRoleDto, UpdateRoleDto } from './dtos';
import { plainToClass } from 'class-transformer';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(RoleRepository)
    private readonly _roleRepository: RoleRepository,
  ) {
    //super( _roleRepository, { useSoftDelete : true });
  }

  async getAll(): Promise<ReadRoleDto[]>{

    const roles = await this._roleRepository.find({
      where: { deletedAt: null },
    });

    return roles.map((role: Role) => plainToClass(ReadRoleDto, role));
  }

  async get(id: number): Promise<ReadRoleDto>{
    if(!id) {
      throw new BadRequestException('id must be sent')
    }

    const role = await this._roleRepository.findOne(id, {
      where: { deletedAt: null },
    });

    if(!role) {
      throw new NotFoundException();
    }

    return plainToClass(ReadRoleDto, role);
  }

  async store(role: Partial<CreateRoleDto>): Promise<ReadRoleDto>{
    const savedRole: Role = await this._roleRepository.save(role);
    return plainToClass(ReadRoleDto, savedRole);
  }

  async update(id: number, role: Partial<UpdateRoleDto>): Promise<ReadRoleDto>{
    const foundRole: Role = await this._roleRepository.findOne(id, {
      where: { deletedAt: null }
    });

    if(!foundRole) {
      throw new NotFoundException('This role does not exist');
    }

    foundRole.name = role.name;
    foundRole.description = role.description;

    const updateRole: Role = await this._roleRepository.save(foundRole);

    return plainToClass(ReadRoleDto, updateRole);
  }

  async destroy(id: number): Promise<void>{
    const roleExists = await this._roleRepository.findOne(id, {
      where: { deletedAt: null }
    });

    if(!roleExists) {
      throw new NotFoundException();
    }

    await this._roleRepository.update(id, {
      deletedAt: new Date()
    });
  }
}
